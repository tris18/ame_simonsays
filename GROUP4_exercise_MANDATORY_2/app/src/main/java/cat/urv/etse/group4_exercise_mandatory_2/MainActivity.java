package cat.urv.etse.group4_exercise_mandatory_2;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    static public BluetoothAdapter mBluetoothAdapter;
    static public BluetoothDevice device;
    static public List<ImageButton> buttons;
    static public boolean inSequence;
    static public List<Drawable> imagesOn;
    static public List<Drawable> imagesOff;
    static public LinkedList<DisplayTask> pendingTasks;
    static public boolean inGame;
    static public List<Score> listScores;
    private List<Integer> sequence;
    private int level;
    private int posicion;
    public static String username;


    /*
        This method initializes the system
     */
    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        buttons = new LinkedList<>();   //List with all the buttons
        imagesOff = new LinkedList<>(); //List with the images of the buttons turned off
        imagesOn = new LinkedList<>();  // List with the images of the buttoons turned on
        listScores = new LinkedList<>(); //HighScores

        /*Load highScores from a file stored in the phone memory*/
        try {
            BufferedReader fin =
                    new BufferedReader(
                            new InputStreamReader(
                                    openFileInput("scores.txt")));

            String texto = fin.readLine();
            while (texto != null) {
                String[] result = texto.split("\\|");
                listScores.add(new Score(result[0], Integer.parseInt(result[1])));
                texto = fin.readLine();
            }
            fin.close();
        } catch (Exception ex) {
            Log.e("Ficheros", "Error al leer fichero desde memoria interna");
        }
        /*Load all necessary resources*/
        buttons.add((ImageButton) findViewById(R.id.greenButton));
        buttons.add((ImageButton) findViewById(R.id.redButton));
        buttons.add((ImageButton) findViewById(R.id.yellowButton));
        buttons.add((ImageButton) findViewById(R.id.blueButton));

        imagesOff.add(getResources().getDrawable(R.drawable.ex_green_off));
        imagesOff.add(getResources().getDrawable(R.drawable.ex_red_off));
        imagesOff.add(getResources().getDrawable(R.drawable.ex_yellow_off));
        imagesOff.add(getResources().getDrawable(R.drawable.ex_blue_off));

        imagesOn.add(getResources().getDrawable(R.drawable.ex_green_on));
        imagesOn.add(getResources().getDrawable(R.drawable.ex_red_on));
        imagesOn.add(getResources().getDrawable(R.drawable.ex_yellow_on));
        imagesOn.add(getResources().getDrawable(R.drawable.ex_blue_on));


        /*Initialization of the game locic necessary variables*/
        inGame = false;
        inSequence = false;
        level = 1;
        pendingTasks = new LinkedList<>();
        sequence = new LinkedList<>();
        /*Enable the bluetooth of the device*/

        /* Start the bluetooth of the phone*/
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            Context context = getApplicationContext();
            CharSequence bluetooth = "El dispositiu no suporta Bluetooth"; //Alert
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, bluetooth, duration);
            toast.show();
        } else if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }

        Intent intent = new Intent(this, BluetoothActivity.class);
        startActivity(intent);
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
        Generates a sequence and starts displays it
     */
    public void displaySequence(View view) {
        if (!inGame && !inSequence) { //We can just display a sequence if it's the moment to do that
            try {
                posicion = 0;
                inSequence = true;
                Random rand = new Random();
                    /*Generation of the sequence*/
                if (level == 1)
                    for (int i = 0; i < 10; i++) {
                        sequence.add(rand.nextInt(4));
                    }
                else
                    sequence.add(rand.nextInt(4));

                pendingTasks = new LinkedList<>();
                for (Integer i : sequence)         //We add to a queue the tasks that will display the sequence
                    pendingTasks.add(new DisplayTask(i));


                pendingTasks.get(0).execute(); //The first task is performed
                pendingTasks.remove(0);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*Processes the effect of clicking the green button*/
    public void buttonGreenOnclick(View view) {
        buttonProcessing(0);
    }

    /*Processes the effect of clicking the red button*/
    public void buttonRedOnclick(View view) {
        buttonProcessing(1);
    }

    /*Processes the effect of clicking the yellow button*/
    public void buttonYellowOnclick(View view) {
        buttonProcessing(2);
    }

    /*Processes the effect of clicking the blue button*/
    public void buttonBlueOnclick(View view) {
        buttonProcessing(3);
    }

    /*Starts the activity that shows the high scores*/
    public void displayHighScores(View view) {
        Intent intent = new Intent(this, HighScores.class);
        startActivity(intent);
    }

    /*Decides if the score is a high score and adds it to the list if necessary*/
    public void addHighScore(int score) {
        int i = 0;
        boolean trobat = false;

        /*Add the score to the list in the first position where it's score is greater than the one before*/
        while (i < listScores.size() && !trobat) {

            if (listScores.get(i).getScore() < score) {
                listScores.add(i, new Score(username, score));
                trobat = true;
            } else i++;

        }
        if (i == listScores.size())
            listScores.add(i, new Score(username, score));

        /*If the list is greater than 10 we remove the worst score*/
        if (listScores.size() > 10)
            listScores.remove(10);

        try {
            /*Update the file*/
            OutputStreamWriter fout =
                    new OutputStreamWriter(
                            openFileOutput("scores.txt", Context.MODE_PRIVATE));

            for (Score s : listScores)
                fout.write(s.getPlayer() + "|" + s.getScore() + "\n");
            fout.close();
        } catch (Exception ex) {
            Log.e("Ficheros", "Error al escribir fichero a memoria interna");
        }
    }

    /*Processes the click of a button during the game
    *int value -> Says which button has been pressed */

    public void buttonProcessing(int value) {
        if (inGame) {
            new ButtonPressed(value).execute(); //Turns on the button for 500 ms
            if (sequence.get(posicion) == value) {  //If the button pressed is the correct in the sequence
                posicion++;
                if (posicion == sequence.size()) { //And all the sequence has been correct we raise the level
                    level++;
                    Context context = getApplicationContext();
                    CharSequence victory = "Felicitats! Has completat el nivell!"; //Alert
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, victory, duration);
                    toast.show();
                    inGame = false;
                    TextView text = (TextView) findViewById(R.id.level); //Change the level in the game
                    text.setText("Level: " + level);
                }
            } else {
                Context context = getApplicationContext(); //If the button pressed was wrong
                CharSequence fail = "Has fallat!";
                int duration = Toast.LENGTH_SHORT; //Display an alert

                Toast toast = Toast.makeText(context, fail, duration);
                toast.show();
                addHighScore(level); //add the score
                level = 1;
                inGame = false;
                TextView text = (TextView) findViewById(R.id.level); //Restart the game from level 1
                text.setText("Level: " + level);
                sequence = new LinkedList<>();
            }
        }
    }
}


