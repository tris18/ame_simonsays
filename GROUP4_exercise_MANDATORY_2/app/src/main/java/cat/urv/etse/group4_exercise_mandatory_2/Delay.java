package cat.urv.etse.group4_exercise_mandatory_2;

import android.os.AsyncTask;

/**
 * Created by Javier Tris on 02/12/2015.
 */
public class Delay extends AsyncTask<Integer, Integer, Integer> {

    private int value;

    public Delay(int value) {
        this.value = value;
    }

    /*Performs a delay between the display of 2 buttons*/
    @Override
    protected Integer doInBackground(Integer... params) {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            return 1;
        }
        return 0;
    }

    /*Starts de displaying of the next element of the sequence,
     if there are no more elements it starts the game*/
    @Override
    protected void onPostExecute(Integer result) {
        MainActivity.buttons.get(value).setBackground(MainActivity.imagesOff.get(value));

        if (!MainActivity.pendingTasks.isEmpty()) {
            MainActivity.pendingTasks.get(0).execute();
            MainActivity.pendingTasks.remove(0);

        } else {
            MainActivity.inGame = true;
            MainActivity.inSequence = false;
        }
    }
}
