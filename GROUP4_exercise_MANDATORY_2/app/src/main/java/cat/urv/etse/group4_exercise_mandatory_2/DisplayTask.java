package cat.urv.etse.group4_exercise_mandatory_2;

import android.bluetooth.BluetoothSocket;
import android.os.AsyncTask;

import java.io.IOException;

public class DisplayTask extends AsyncTask<Integer, Integer, Integer> {

    private int value;

    public DisplayTask(int value) {
        this.value = value;
    }

    @Override
    /*
        Turns on the current button
     */
    protected void onPreExecute() {

        MainActivity.buttons.get(value).setBackground(MainActivity.imagesOn.get(value));
        String s;
        switch (value){
            case 0:
                s="G";
                break;
            case 1:
                s="R";
                break;
            case 2:
                s="Y";
                break;
            case 3:
                s = "B";
                break;
            default:
                s="";
        }
        try {
            BluetoothActivity.outputStream.write(s.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*Keep the button on for half a second*/
    @Override
    protected Integer doInBackground(Integer... params) {
        try {
            Thread.sleep(600);
        } catch (InterruptedException e) {
            return 1;
        }
        return 0;
    }

    /*Turn off the current button*/
    @Override
    protected void onPostExecute(Integer result) {
        MainActivity.buttons.get(value).setBackground(MainActivity.imagesOff.get(value));

        Delay d = new Delay(value);
        d.execute();
    }
}
