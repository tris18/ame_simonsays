package cat.urv.etse.group4_exercise_mandatory_2;

import android.os.AsyncTask;

import java.io.IOException;

public class ButtonPressed extends AsyncTask<Integer, Integer, Integer> {

    private int value;

    public ButtonPressed(int value) {
        this.value = value;
    }

    @Override
    /*Turn on the button*/
    protected void onPreExecute() {
        MainActivity.buttons.get(value).setBackground(MainActivity.imagesOn.get(value));

        String s;
        switch (value) {
            case 0:
                s = "G";
                break;
            case 1:
                s = "R";
                break;
            case 2:
                s = "Y";
                break;
            case 3:
                s = "B";
                break;
            default:
                s = "";
        }
        try {
            BluetoothActivity.outputStream.write(s.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    /*Make the button stay on*/
    protected Integer doInBackground(Integer... params) {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            return 1;
        }
        return 0;
    }

    @Override
    /*Turn off the button*/
    protected void onPostExecute(Integer result) {
        MainActivity.buttons.get(value).setBackground(MainActivity.imagesOff.get(value));
    }
}