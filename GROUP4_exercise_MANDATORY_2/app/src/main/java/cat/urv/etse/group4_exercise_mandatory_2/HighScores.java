package cat.urv.etse.group4_exercise_mandatory_2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

public class HighScores extends AppCompatActivity {
    /*Displays the high scores*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_scores);
        List<TextView> list = new LinkedList<>();
        list.add((TextView) findViewById(R.id.text1));
        list.add((TextView) findViewById(R.id.text2));
        list.add((TextView) findViewById(R.id.text3));
        list.add((TextView) findViewById(R.id.text4));
        list.add((TextView) findViewById(R.id.text5));
        list.add((TextView) findViewById(R.id.text6));
        list.add((TextView) findViewById(R.id.text7));
        list.add((TextView) findViewById(R.id.text8));
        list.add((TextView) findViewById(R.id.text9));
        list.add((TextView) findViewById(R.id.text10));

        for (int i = 0; i < list.size(); i++) {
            list.get(i).setText(i + 1 + "-");
        }

        for (int i = 0; i < MainActivity.listScores.size(); i++) {
            list.get(i).setText(i + 1 + "-" + MainActivity.listScores.get(i).getPlayer()
                    + "-" + MainActivity.listScores.get(i).getScore());
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_high_scores, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
