package cat.urv.etse.group4_exercise_mandatory_2;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

public class BluetoothActivity extends AppCompatActivity {
    ListView lv;
    private ArrayAdapter<String> BTArrayAdapter;
    private BluetoothDevice device;
    static public OutputStream outputStream;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);
        lv = (ListView) findViewById(R.id.listView);

        /*When we choose the MAC of a bluetooth reciever we create a socket to connect
        with that device
         */
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                MainActivity.mBluetoothAdapter.cancelDiscovery();
                Object o = lv.getItemAtPosition(position);
                String str = o.toString();//As you are using Default String Adapter
                device = MainActivity.mBluetoothAdapter.getRemoteDevice(o.toString()); //get the device
                MainActivity.mBluetoothAdapter.cancelDiscovery();
               device.createBond();
                try {
                    //Create the socket and connect with the device
                    BluetoothSocket socket = device.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
                    socket.connect();
                    outputStream = socket.getOutputStream(); //We obtain the otputStream in order to be able to send data to the receiver
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Toast.makeText(getBaseContext(), str, Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        BTArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        lv.setAdapter(BTArrayAdapter);

        // We have to discover the devices that are near us and select the one we want to connect with
        if (MainActivity.mBluetoothAdapter.isDiscovering()) {
            // the button is pressed when it discovers, so cancel the discovery
            MainActivity.mBluetoothAdapter.cancelDiscovery();
        } else {
            BTArrayAdapter.clear();
            MainActivity.mBluetoothAdapter.startDiscovery();


                registerReceiver(bReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_bluetooth, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    final BroadcastReceiver bReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // add the name and the MAC address of the object to the arrayAdapter
                BTArrayAdapter.add(device.getAddress());
                BTArrayAdapter.notifyDataSetChanged();
            }
        }
    };

}
